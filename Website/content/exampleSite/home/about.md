+++
# About/Biography widget.
widget = "about"
active = true
date = 2018-09-5T00:00:00

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Cognitive Science",
    "Scientometrics",
    "Language and Cognition"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "MA in Philosophy"
  institution = "Universidad de Chile"
  year = 2016

[[education.courses]]
  course = "BA in Philosophy"
  institution = "Universidad de Chile"
  year = 20012
 
+++

# Biography

Hey! I'm a Ph. D. Student at the Department of Psychology, Cornell University. I'm a member of the [Cognitive Neuroscience Lab](http://cnl.psych.cornell.edu) at Cornell University and the [Co-Mind Lab](http://co-mind.org/people.html) at the University of California, Los Angeles. My main research interest lie in the foundations of modern culturally-transformed human cognition, especially through symbol use, language and categorization. Having a background in Philosophy of Science, I'm also interested in data-driven exploration of scientific activity. I like to tackle these topics through a combination of theory-driven hypothesis formation and computational data analysis.
